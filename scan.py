import os
from threading import Thread
import argparse

parser = argparse.ArgumentParser(description='Scan files')
parser.add_argument('-d', '--directory', type=str,
                    help='a directory to scan')
parser.add_argument('-s', '--signature', type=str,
                    help='byte signature to find')
parser.add_argument('-t', '--threads', type=int, help='count of threads, optional', default=0)


# кол-во потоков пусть будет кол-во потоков проца * 2 и на это число поделить кол-во файлов в директории и
# субдиректориях но не меньше этого числа либо если файлов меньше то ровно столько, сколько файлов
class ScanningThread(Thread):
    def __init__(self, name, files, signature, result_list):
        super().__init__(name=name, group=None)
        self.name = name
        self.files = files
        self.signature = signature
        self.scanned_files = {}
        self.result_list = result_list

    def run(self):
        for file in self.files:
            with open(file, 'rb') as f:
                is_found = self.scan(f)
                self.scanned_files[file] = is_found
        self.result_list.append(self.scanned_files)

    def scan(self, file):
        while True:
            buf = file.read(8192)
            if buf:
                add_buf = file.read(len(self.signature))
                if add_buf:
                    old_pointer = file.tell()
                    found_index = bytes.find(buf + add_buf, self.signature)
                    file.seek(old_pointer, 0)
                else:
                    found_index = bytes.find(buf, self.signature)
                if found_index != -1:
                    return True
            else:
                break
        return False


def get_all_filenames_from_directory(dir):
    list_of_files = []
    for (dirpath, dirnames, filenames) in os.walk(dir):
        list_of_files += [os.path.join(dirpath, file) for file in filenames]
    return list_of_files


def calculate_thread_count(files_count):
    optimal_count = os.cpu_count() * 2  # тут так сказать от балды, вроде как io-bound и потоков можно достаточно
    # много делать, так почему бы и не вдвое больше чем логических процессоров?
    if files_count <= optimal_count:
        return files_count
    return optimal_count + (files_count // optimal_count)


def start_scan():
    parsed_args = parser.parse_args()
    directory = parsed_args.directory
    signature = parsed_args.signature
    thread_count = parsed_args.threads

    byte_signature = bytes.fromhex(signature)

    list_of_files = get_all_filenames_from_directory(directory)
    if thread_count == 0:
        thread_count = calculate_thread_count(len(list_of_files))
    elif thread_count > len(list_of_files):
        thread_count = len(list_of_files)
    scanned_files_list = []
    threads = []
    files_per_thread = len(list_of_files) // thread_count
    for i in range(thread_count):
        new_thread = ScanningThread(name=f'thread_{i}',
                                    files=list_of_files[i*files_per_thread:i*files_per_thread+files_per_thread],
                                    signature=byte_signature,
                                    result_list=scanned_files_list)
        threads.append(new_thread)

    rest_of_files = len(list_of_files) % thread_count
    if rest_of_files != 0:
        new_thread = ScanningThread(name='last_thread',
                                    files=list_of_files[-rest_of_files:],
                                    signature=byte_signature,
                                    result_list=scanned_files_list)
        threads.append(new_thread)

    [t.start() for t in threads]
    [t.join() for t in threads]

    with open('result_file.txt', 'w') as result_file:
        for dictionary in scanned_files_list:
            for key, value in dictionary.items():
                result_file.write((f'{key} - detected' if value else f'{key} - undetected') + os.linesep)


if __name__ == '__main__':
    start_scan()